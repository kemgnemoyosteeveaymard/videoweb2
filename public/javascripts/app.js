/*  region variable declaration */

let OV; // OpenVidu object to initialize a session
let session; // Session object where the user will connect
let publisher; // Publisher object which the user will publish
let sessionId; // Unique identifier of the session
let audioEnabled = true; // True if the audio track of publisher is active
let videoEnabled = true; // True if the video track of publisher is active
let numOfVideos = 0; // Keeps track of the number of videos that are being shown
let recordingVideo = false; // True if the video are recording

let baseUrl = "https://api.testedoctar.ml";
if((/^http\:\/\/localhost/.test(window.location.href))|| (/^http\:\/\/192/.test(window.location.href))){
  baseUrl = 'http://localhost:1337'; // for running/testing locally
}

const joinButton = document.querySelector('#joinButton');

/* const playCircle = document.createElement('i');
playCircle.classList.add(['fas', 'fa-play-circle']); */
const playCircle = `fa-play-circle`;

/* const stopCircle = document.createElement('i');
stopCircle.classList.add(['fas', 'fa-stop-circle']); */
const stopCircle = `fa-stop-circle`;

const video = `<i class="fas fa-video"></i>`;
const videoSlash = `<i class="fas fa-video-slash"></i>`;

const microphoneSlash = `<i class="fas fa-microphone-alt-slash"></i>`;
const microphone = `<i class="fas fa-microphone-alt"></i>`;

const bluring = document.querySelector(".bluring");
const bluringText = document.querySelector(".bluring-text");
const btnRound = document.querySelectorAll(".btn-round");
const actionButtons = document.querySelectorAll(".action-button");

const hour = document.querySelector("#hour");
const minute = document.querySelector("#minute");
const second = document.querySelector("#second");
const recordCounter = document.querySelector(".record-counter");

const type_msg = document.querySelector(".type_msg");
const send_btn = document.querySelector(".send_btn");
const msg_card_body = document.querySelector(".msg_card_body");
const notif_chat = document.querySelector('.notif_chat');
const openButton = document.getElementById('open-button');
const closeButton = document.getElementById('closeButton');
const chatBox = document.getElementById('myForm');
let numberMessages = 0;

let startTimeRecord;
let stopTimeRecord;
let ellapsedTime;
let recordCounterInterval;

let myConnection;

/*  endregion variable declaration  */

// Check if the URL already has a room
window.addEventListener("load", function() {
  sessionId = window.location.hash.slice(1); // For 'https://myurl/#roomId', sessionId would be 'roomId'
  if (sessionId) {
    // The URL has a session id. Join the room right away
    console.log("Joining to room " + sessionId);
    showSessionHideJoin();
    joinRoom();
  } else {
    // The URL has not a session id. Show welcome page
    showJoinHideSession();
  }

  actionButtons.forEach(btn => {
    btn.addEventListener("click", handleActionButton);
  });

  openButton.addEventListener('click', openChatBox);
  closeButton.addEventListener('click', closeChatBox);
  joinButton.addEventListener('click', joinRoom);  
});

// Disconnect participant on browser's window closed
window.addEventListener("beforeunload", function() {
  if (session){
    session.disconnect();    
  }
});

function joinRoom() {
  //  Show overlay when click on join room
  showBluring();

  // --- 1) Get an OpenVidu object ---

  OV = new OpenVidu();

  // --- 2) Init a session ---

  session = OV.initSession();

  // --- 3) Specify the actions when events take place in the session ---

  // On every new Stream received...
  session.on("streamCreated", function(event) {
    // Subscribe to the Stream to receive it. HTML video will be appended to element with 'subscriber' id

    bluringText.textContent = "Event on stream created";

    console.log("Event on stream created", event.stream);
    var subscriber = session.subscribe(event.stream, "videos");
    // When the new video is added to DOM, update the page layout to fit one more participant
    subscriber.on("videoElementCreated", function(event) {
      console.log("video element created");
      numOfVideos++;
      showSessionHideJoin();
      updateLayout();
    });
  });

  // On every new Stream destroyed...
  session.on("streamDestroyed", function(event) {
    // Update the page layout
    console.log("stream destroyed");
    numOfVideos--;
    updateLayout();
    leaveWindow();
  });

  session.on("sessionDisconnected", event => {
    console.log("sessionDisconnected");
    leaveWindow();
  });

  session.on("signal:edoctar-chat", event => {
    console.log("receiveMessage", event);
    receiveMessage(event);
  });

  session.on("connectionCreated", event => {
    myConnection = event.connection;
    console.log("myConnection", myConnection);
  });

  // --- 4) Connect to the session with a valid user token ---

  // 'getToken' method is simulating what your server-side should do.
  // 'token' parameter should be retrieved and returned by your own backend
  getToken(sessionId)
    .then(response => {
      // Connect with the token
      const token = response.token;
      sessionId = response.sessionName;

      session
        .connect(token)
        .then(() => {
          // --- 5) Set page layout for active call ---

          // Update the URL shown in the browser's navigation bar to show the session id
          const path =
            location.pathname.slice(-1) == "/"
              ? location.pathname
              : location.pathname + "/";
          window.history.pushState("", "", path + "#" + sessionId);

          // Auxiliary methods to show the session's view
          initializeSessionView();

          // --- 6) Get your own camera stream with the desired properties ---

          publisher = OV.initPublisher(
            "publisher",
            {
              audioSource: undefined, // The source of audio. If undefined default audio input
              videoSource: undefined, // The source of video. If undefined default video input
              publishAudio: true, // Whether to start publishing with your audio unmuted or not
              publishVideo: true, // Whether to start publishing with your video enabled or not
              resolution: "640x480", // The resolution of your video
              frameRate: 30, // The frame rate of your video
              insertMode: "APPEND", // How the video is inserted in target element 'video-container'
              mirror: true // Whether to mirror your local video or not
            },
            error => {
              if (error) {
                showJoinHideSession();
              } else {
                console.log("Publisher successfully initialized");
              }
            }
          );

          // --- 7) Specify the actions when events take place in our publisher ---

          // When our HTML video has been added to DOM...
          publisher.on("videoElementCreated", function(event) {
            // When your own video is added to DOM, update the page layout to fit it
            console.log("video element created");
            hideBluring();
            showSessionHideJoin();

            numOfVideos++;
            updateLayout();
            $(event.element).prop("muted", true); // Mute local video to avoid feedback
          });

          // --- 8) Publish your stream ---

          session.publish(publisher);
        })
        .catch(error => {
          console.log(
            "There was an error connecting to the session:",
            error.code,
            error.message
          );          
          showJoinHideSession();
        });
    })
    .catch(e => {
      console.warn(e);
      showJoinHideSession();
      leaveWindow();
    });
}

function leaveRoom() {
  // --- 9) Leave the session by calling 'disconnect' method over the Session object ---

  session.disconnect();

  // Back to welcome page
  window.location.href = window.location.origin + window.location.pathname;
}

function leaveWindow(){
  window.location.href = window.location.origin + window.location.pathname;
}

/* AUXILIARY MEHTODS */

function handleActionButton(e) {
  if (this.classList.contains("disabled")) {
    return;
  } else {
    this.classList.toggle("active");
    const data = this.dataset.action;
    switch (data) {
      case "muteAudio":
        muteAudio(this);
        break;
      case "muteVideo":
        muteVideo(this);
        break;
      case "recordVideo":
        recordVideo(this);
        break;
      default:
        return;
    }
  }
}

function muteAudio(elt) {
  var messageTooltip;
  audioEnabled = !audioEnabled;
  publisher.publishAudio(audioEnabled);

  if (!audioEnabled) {
    elt.innerHTML = microphoneSlash;
    messageTooltip = "Audio Off";
  } else {
    elt.innerHTML = microphone;
    messageTooltip = "Audio On";
  }
  $("#muteAudio")
    .attr("title", messageTooltip)
    .attr("data-original-title", messageTooltip)
    .tooltip("update")
    .tooltip("show");
}

function muteVideo(elt) {
  var messageTooltip;
  videoEnabled = !videoEnabled;
  publisher.publishVideo(videoEnabled);

  if (!videoEnabled) {
    elt.innerHTML = videoSlash;
    messageTooltip = "Video Off";
  } else {
    elt.innerHTML = video;
    messageTooltip = "Video On";
  }
  $("#muteVideo")
    .attr("title", messageTooltip)
    .attr("data-original-title", messageTooltip)
    .tooltip("update")
    .tooltip("show");
}

function recordVideo(elt) {
  const child = elt.childNodes[1];
  recordingVideo = !recordingVideo;
  //publisher.publishVideo(videoEnabled);

  if (recordingVideo) {
    child.classList.remove(playCircle);
    child.classList.add(stopCircle);

    startTimeRecord = new Date();
    recordCounterInterval = setInterval(handleRecordCounter, 1000);
    recordCounter.classList.add("active");
    messageTooltip = "Start Recording";
    startRecording(sessionId)
      .then(response => {
        console.log(messageTooltip);
      })
      .catch(err => {
        console.error(err);
      });
  } else {
    child.classList.remove(stopCircle);
    child.classList.add(playCircle);
    clearInterval(recordCounterInterval);
    messageTooltip = "Stop Recording";
    /* $("#recordVideo")
      .attr("title", messageTooltip)
      .attr("data-original-title", messageTooltip)
      .tooltip("update")
      .tooltip("show"); */

    stopRecording(sessionId)
      .then(response => {
        console.log(messageTooltip);
      })
      .catch(err => {
        console.error(err);
      });
  }
}

function handleRecordCounter() {
  stopTimeRecord = new Date();
  ellapsedTime = (stopTimeRecord - startTimeRecord) / 1000;

  const secondTime = Math.floor(ellapsedTime % 60);
  const minuteTime = Math.floor(ellapsedTime / 60) % 60;
  const hourTime = Math.floor(ellapsedTime / 3600);

  secondTime <= 9
    ? (second.textContent = `0${secondTime}`)
    : (second.textContent = secondTime);
  minuteTime <= 9
    ? (minute.textContent = `0${minuteTime}`)
    : (minute.textContent = minuteTime);
  hourTime <= 9
    ? (hour.textContent = `0${hourTime}`)
    : (hour.textContent = hourTime);
}

function randomString() {
  return Math.random()
    .toString(36)
    .slice(2);
}

// 'Session' page
function showSessionHideJoin() {
  $("#join").hide();
  $("#session").show();
  $("footer").hide();
  btnRound.forEach(btn => btn.classList.remove("disabled"));
  /* if (numOfVideos >= 1) {
    $('li[data-action="recordVideo"]')[0].addClass("disabled");
  } */
}

// 'Join' page
function showJoinHideSession() {
  $("#join").show();
  $("#session").hide();
  $("footer").show();
  $("#joinButton").attr("disabled", false);
  btnRound.forEach(btn => btn.classList.add("disabled"));
  hideBluring();
  console.log("session",session);  
}

//  Bluring page
function showBluring() {
  //bluring.style.transform = `translateX(0px)`;
  bluring.style.display = "flex";
}

function hideBluring() {
  //bluring.style.transform = `translateX(2000px)`;
  bluring.style.display = "none";
}

// Prepare HTML dynamic elements (URL clipboard input)
function initializeSessionView() {
  // Tooltips
  $('[data-toggle="tooltip"]').tooltip();

  // Input clipboard
  $("#copy-input").val(window.location.href);
  $("#copy-button").bind("click", function() {
    const input = document.getElementById("copy-input");
    input.focus();
    input.setSelectionRange(0, input.value.length);
    try {
      const success = document.execCommand("copy");
      if (success) {
        $("#copy-button").trigger("copied", ["Copied!"]);
      } else {
        $("#copy-button").trigger("copied", ["Copy with Ctrl-c"]);
      }
    } catch (err) {
      $("#copy-button").trigger("copied", ["Copy with Ctrl-c"]);
    }
  });

  // Handler for updating the tooltip message.
  $("#copy-button").bind("copied", function(event, message) {
    $(this)
      .tooltip("hide")
      .attr("title", message)
      .attr("data-original-title", message)
      .tooltip("update")
      .tooltip("show")
      .attr("title", "Copy to Clipboard")
      .attr("data-original-title", "Copy to Clipboard")
      .tooltip("update")
      .addClass("active");
  });

  //  Chat Box
  send_btn.addEventListener("click", sendMessage);
}

// Dynamic layout adjustemnt depending on number of videos
function updateLayout() {
  console.warn("There are now " + numOfVideos + " videos");

  const publisherDiv = $("#publisher");
  const publisherVideo = $("#publisher video");
  const subscriberVideos = $("#videos > video");

  publisherDiv.removeClass();
  publisherVideo.removeClass();
  subscriberVideos.removeClass();

  switch (numOfVideos) {
    case 1:
      publisherVideo.addClass("video1");
      break;
    case 2:
      publisherDiv.addClass("video2");
      subscriberVideos.addClass("video2");
      break;
    case 3:
      publisherDiv.addClass("video3");
      subscriberVideos.addClass("video3");
      break;
    case 4:
      publisherDiv.addClass("video4");
      publisherVideo.addClass("video4");
      subscriberVideos.addClass("video4");
      break;
    default:
      publisherDiv.addClass("videoMore");
      publisherVideo.addClass("videoMore");
      subscriberVideos.addClass("videoMore");
      break;
  }
}

/**
 * --------------------------
 * CHAT-SIDE RESPONSABILITY
 * --------------------------
 * These methode allow user to retrieve token to enter a session
 *   1) Send message to everyone
 *   2) Receive Message from everyone
 *   3) Function to display send and receive message
 */

function sendMessage() {    
  const messageValue = type_msg.value;
  if (session && messageValue.trim().length > 0) {
    session
      .signal({
        data: messageValue, // Any string (optional)
        to: [], // Array of Connection objects (optional. Broadcast to everyone if empty)
        type: "edoctar-chat" // The type of message (optional)
      })
      .then(() => {
        displaySendMessage(messageValue);
        type_msg.value = "";
      })
      .catch(error => {
        console.error(error);
      });
  } else {
    console.log("can't send message, session or no message to send");
  }

  console.group('sendMessage');
  console.log(myConnection);
  console.log(messageValue);
  console.groupEnd();
}

function receiveMessage(event) {  

  console.group('receiveMessage');
  console.log(myConnection);
  console.log("remoteConnection", myConnection.session.remoteConnections);
  console.log(event);
  if(isEmpty(myConnection.session.remoteConnections)){
    console.log('cool')
  }else{
    console.log('pas cool');
  }
  console.groupEnd();
 

  if (myConnection.connectionId !== event.from.connectionId || isEmpty(myConnection.session.remoteConnections)) {
    return;
  } else {
    numberMessages++;
    notif_chat.textContent = numberMessages;
    displayReceiveMessage(event.data);
  }
}

function displaySendMessage(message) {
  const messageHtml = `
  <div class="d-flex justify-content-end mb-2">
    <div class="msg_cotainer_send">
      ${message}         
    </div>
    <div class="img_cont_msg">
      <img src="/images/default-user.jpg" class="rounded-circle user_img_msg">
    </div>
  </div>`;

  msg_card_body.innerHTML += messageHtml;
  $(".msg_card_body").animate(
    {
      scrollTop: $(".msg_card_body > div")
        .last()
        .offset().top
    },
    2000
  );
}

function displayReceiveMessage(message) {
  const messageHtml = `
  <div class="d-flex justify-content-start mb-2">
    <div class="msg_cotainer">
      ${message}
    </div>
    <div class="img_cont_msg">
      <img src="/images/default-user.jpg" class="rounded-circle user_img_msg">
    </div>
  </div>`;

  msg_card_body.innerHTML += messageHtml;
  $(".msg_card_body").animate(
    {
      scrollTop: $(".msg_card_body > div")
        .last()
        .offset().top
    },
    2000
  );
}

function openChatBox(){
  openButton.style.display = 'none';
  chatBox.style.display = 'block';
  if(numberMessages > 0){
    notif_chat.textContent = 0;
    numberMessages = 0;
  }
}

function closeChatBox(){
  chatBox.style.display = 'none';
  openButton.style.display = 'block';
}

/**
 * --------------------------
 * SERVER-SIDE RESPONSIBILITY
 * --------------------------
 * These methode allow user to retrieve token to enter a session
 *   1) Initialize a session in OpenVidu Server	(POST /api/sessions)
 *   2) Generate a token in OpenVidu Server		(POST /api/tokens)
 *   3) The token must be consumed in Session.connect() method
 */

function getToken(sessionId) {
  return new Promise((resolve, reject) => {
    httpPostRequest(
      baseUrl + "/api-sessions/get-token",
      { sessionId: sessionId },
      "Request of TOKEN gone Wrong"
    )
      .then(response => {
        resolve(response);
        console.warn(
          "Request of TOKEN gone WELL (TOKEN:" + response.token + ")"
        );
        bluringText.textContent =
          "Token for accesing session video have been created";
      })
      .catch(e => {
        reject(e);
      });
  });
}

function startRecording(sessionId) {
  return new Promise((resolve, reject) => {
    httpPostRequest(
      baseUrl + "/api-sessions/start-recording",
      { sessionId: sessionId },
      "Recording session gone well"
    )
      .then(response => {
        resolve(response);
        console.warn("Recording session gone well");
      })
      .catch(e => {
        reject(e);
      });
  });
}

function stopRecording(sessionId) {
  return new Promise((resolve, reject) => {
    httpPostRequest(
      baseUrl + "/api-sessions/stop-recording",
      { sessionId: sessionId },
      "Recording session gone well"
    )
      .then(response => {
        resolve(response);
        console.warn("Recording session gone well");
      })
      .catch(e => {
        reject(e);
      });
  });
}

function httpPostRequest(url, body, errorMsg) {
  return new Promise((resolve, reject) => {
    const http = new XMLHttpRequest();
    http.open("POST", url, true);
    http.setRequestHeader("Content-type", "application/json");
    http.addEventListener("readystatechange", processRequest, false);
    http.send(JSON.stringify(body));

    function processRequest() {
      if (http.readyState == 4) {
        if (http.status == 200) {
          try {
            resolve(JSON.parse(http.responseText));
          } catch (e) {
            reject(e);
          }
        } else {
          reject(errorMsg);
          console.warn(errorMsg);
          console.warn(http.responseText);
        }
      }
    }
  });
}


//  Prototype
const isEmpty = function(obj) {
  for(var key in obj) {
      if(obj.hasOwnProperty(key))
          return false;
  }
  return true;
}